# README #

This README would normally document whatever steps are necessary to get your application up and running.
For now I'm just letting you know that I will be building this app for Heroku hosting.

### What is this repository for? ###

* An anonymous suggestion box so that co-workers can raise issues, complaints and suggestions anonymously to management. This platform will also serve as a way for co-workers to agree / disagree with the raised issues in the form of voting.
* Version 0.1.0
    * [Semantic Versioning 2.0] used for version numbering
* [Learn Markdown] and [Markdown Cheatsheet]

### How do I get set up? ###

* Summary of set up: [Jetbrains IntelliJ Idea] IDE used with a [Python 3.5] vitual environment
* Configuration: ToDo
* Dependencies: Pip requirement file included for dependencies
* Database configuration: [MongoDB] in use
* How to run tests: [unittest] in use for testing
* Deployment instructions: [Heroku]

### Contribution guidelines ###

* Writing tests: [Writing Tests] from the Hitchhickers Guide to Python consulted for test writing
* Code review: ToDo - Possible service to use: [Quantifiedcode]
* Other guidelines: [Sphinx] used for documentation generation.

### Who do I talk to? ###

* [L33tCh] or [Jeff]

[Semantic Versioning 2.0]:(http://semver.org/)
[Learn Markdown]:(https://bitbucket.org/tutorials/markdowndemo)
[Markdown Cheatsheet]:(https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
[Jetbrains IntelliJ Idea]:(https://www.jetbrains.com/idea/specials/idea/idea.html) 
[Python 3.5]:(https://docs.python.org/3.5/)
[MongoDB]:(https://www.mongodb.com/)
[unittest]:(https://docs.python.org/3/library/unittest.html)
[Heroku]:(http://www.heroku.com/)
[Writing Tests]:(http://docs.python-guide.org/en/latest/writing/tests/)
[Quantifiedcode]:(https://www.quantifiedcode.com/)
[Sphinx]:(http://www.sphinx-doc.org/en/stable/)
[L33tCh]:(https://bitbucket.org/L33tCh/)
[Jeff]:(https://bitbucket.org/aaX_Jeff/)